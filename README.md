# StorageStats v1.2

Display a basic quantity graph for any storage for the last 5 minutes in [Autonauts](https://store.steampowered.com/app/979120/Autonauts/).

* Steam Workshop: https://steamcommunity.com/sharedfiles/filedetails/?id=2747077746
* Discussion on reddit: https://www.reddit.com/r/Autonauts/comments/snjpz8/storagestats_mod_to_display_a_graph_next_to_your/
* Chat on Discord: https://discord.com/channels/331886561044201473/647074670822162440/940592994355146832

https://gitlab.com/parhuzamos/storagestats

![StorageStats](textures/StorageStats.jpg)

Place this sign next to any storage to see on a graph how the stored quantity changed in the last 5 minutes. 
Connect the sign by placing it next to a storage (1 tile left, top, right, bottom). 
After the broken link disappears the storage and the sign are connected, a graph for that storage will be displayed. 
You may move the sign elsewhere(!), they are connected forever and title of the sign contains the storage's name. 
After connecting, the sign doesn't need to be placed next to the storage anymore. 
Any number of signs can be connected to a storage (and placed anywhere).

Using [Broken Link](https://icons8.com/icon/65949/broken-link) icon by [Icons8](https://icons8.com).

https://gitlab.com/parhuzamos/storagestats

## Changelog

### [v1.2] - 2023-08-26
Fix for Autonauts v140.2 : proper display of textures instead of just pink color. Thanks [Kodijack!](https://steamcommunity.com/id/kodijack)!

### [v1.1] - 2022-02-16
Minor updates:
* Sign's name updated according to the storage's name. Thanks **knito** for the idea.
* Signs are rotated correctly, previous savegames are not affected.

### [v1.0] - 2022-02-08
First release.

### [v0.9] - 2022-02-08
Initial version.

## Todos

* Display the icon of the stored stuff (the storage's icon) next to the sign (below, above)
* Indicate linked state with an icon on the base plate
* Remove base plate? Or show in red if not linked?
* Configure the time period for the graph
* Large version where you can see multiple graphs at once. 
    Place the big-sign (billboard/building) next to a storage, it connects. 
    Move it to another and repeat that to fill up the graph slots (3x4? slots). 
    Place next to an unassigned storage (which never held anything) to remove the last added storage from the last slot. 
    Graphics could be like a computer or stock market display. 
    The storage's icon next to each graph would be nice.
    
    Items to display:
    * log
    * plank
    * pole
    * stone
    * clay
    * charcoal
    * coal
    * metal ore
    * crude metal
    
        
    
