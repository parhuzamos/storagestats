#!/bin/bash
# Created by bas v0.6.1

function help {
cat <<EOF
$(basename "${0}") v0.1 - 

Usage:
    $(basename "${0}") <directory>

Params:
    directory                         this contains "models" and "textures"

Dependencies:
    convert - image magick

EOF
}

if [[ "$1" == "--help" || "$1" == "-h" ]]; then
    help;
    exit 1;
fi;
if [[ "$#" -eq 0 ]]; then
    help;
    exit 1;
fi;

function header {
    count=$(( $1 + 1 ));
    cat <<EOF
# Material Count: ${count}

EOF
}

function block {
    block_name=$1
    cat <<EOF
newmtl ${block_name}
Ns 225.000000
Ka 1.000000 1.000000 1.000000
Kd 0.800000 0.800000 0.800000
Ks 0.000000 0.000000 0.000000
Ke 0.000000 0.000000 0.000000
Ni 0.000000
d 1.000000
illum 2
map_Kd textures/${block_name}.png
EOF
}

PERCENT_LOW_QUANTITY=10
PERCENT_HIGH_QUANTITY=90

function texture {
    dir=$1;
    prefix=$2;
    index=$3;
    image="${dir}${prefix}${index}.png";
    color="lightblue";
    if [[ "${index}" -le PERCENT_LOW_QUANTITY ]]; then
        color="red";
    elif [[ "${index}" -ge PERCENT_HIGH_QUANTITY ]]; then
        color="green";
    fi

#convert \
#	-background ${color} \
#	-fill black \
#	-size 20x64 \
#	-pointsize 10 \
#	-gravity center \
#	"label:${index}%" \
#    "${image}"

#    convert -size 20x100 "xc:${color}" "${image}"
    bottom=$(bc <<<"scale=2; 50 + (50 / 100 * ${index})")
    convert -size 40x200 canvas:white -draw "fill ${color} rectangle 25,50 34,${bottom}" "${image}"
}

function object_header {
    name=$1
    cat <<EOF
# Blender v2.82 (sub 7) OBJ File: '${name}.blend'
# www.blender.org
mtllib ${name}.mtl
o Base
v 1.000000 0.200000 -1.000000
v -1.000000 0.200000 -1.000000
v -1.000000 0.200000 1.000000
v 1.000000 0.200000 1.000000
v 1.000000 0.100000 1.000000
v -1.000000 0.100000 1.000000
v -1.000000 0.100000 -1.000000
v 1.000000 0.100000 -1.000000
vt 0.625000 0.500000
vt 0.875000 0.500000
vt 0.875000 0.750000
vt 0.625000 0.750000
vt 0.375000 0.750000
vt 0.625000 1.000000
vt 0.375000 1.000000
vt 0.375000 0.000000
vt 0.625000 0.000000
vt 0.625000 0.250000
vt 0.375000 0.250000
vt 0.125000 0.500000
vt 0.375000 0.500000
vt 0.125000 0.750000
vn 0.0000 1.0000 0.0000
vn 0.0000 0.0000 1.0000
vn -1.0000 0.0000 0.0000
vn 0.0000 -1.0000 -0.0000
vn 1.0000 0.0000 0.0000
vn 0.0000 0.0000 -1.0000
usemtl Material.002
s 1
f 1/1/1 2/2/1 3/3/1 4/4/1
f 5/5/2 4/4/2 3/6/2 6/7/2
f 6/8/3 3/9/3 2/10/3 7/11/3
f 7/12/4 8/13/4 5/5/4 6/14/4
f 8/13/5 1/1/5 4/4/5 5/5/5
f 7/11/6 2/10/6 1/1/6 8/13/6
EOF
}

function cube {
    index=$1;
    num=$(( index -1 ));
    name="Cube${index}";
    material="mat${index}";
    shift;

    diff=$(( num * 6000 ));
    c1=$(( $1 - diff ));
    c2=$(( $2 - diff ));
    c3=$(( $3 - diff ));
    c4=$(( $4 - diff ));
    c5=$(( $5 - diff ));
    c6=$(( $6 - diff ));
    c7=$(( $7 - diff ));
    c8=$(( $8 - diff ));

    cat <<EOF
o Cube${index}
v -0.$c1 0.350000 -0.950000
v -0.$c2 0.350000 -0.950000
v -0.$c3 0.350000 -0.050000
v -0.$c4 0.350000 -0.050000
v -0.$c5 0.310000 -0.050000
v -0.$c6 0.310000 -0.050000
v -0.$c7 0.310000 -0.950000
v -0.$c8 0.310000 -0.950000
vt 0.625000 0.500000
vt 0.875000 0.500000
vt 0.875000 0.750000
vt 0.625000 0.750000
vt 0.375000 0.750000
vt 0.625000 1.000000
vt 0.375000 1.000000
vt 0.375000 0.000000
vt 0.625000 0.000000
vt 0.625000 0.250000
vt 0.375000 0.250000
vt 0.125000 0.500000
vt 0.375000 0.500000
vt 0.125000 0.750000
vn 0.0000 1.0000 0.0000
vn 0.0000 0.0000 1.0000
vn -1.0000 0.0000 0.0000
vn 0.0000 -1.0000 -0.0000
vn 1.0000 0.0000 0.0000
vn 0.0000 0.0000 -1.0000
usemtl ${material}
s 1
EOF
}

function face {
    index=$(( $1 - 1 ));
    shift;

    c1=$(( $1 + index*8 ));
    c2=$(( $2 + index*14 ));
    c3=$(( $3 + index*5 ));
    c4=$(( $4 + index*8 ));
    c5=$(( $5 + index*14 ));
    c6=$(( $6 + index*5 ));
    c7=$(( $7 + index*8 ));
    c8=$(( $8 + index*14 ));
    c9=$(( $9 + index*5 ));
    c10=$(( ${10} + index*8 ));
    c11=$(( ${11} + index*14 ));
    c12=$(( ${12} + index*5 ));
    cat <<EOF
f $c1/$c2/$c3 $c4/$c5/$c6 $c7/$c8/$c9 $c10/$c11/$c12
EOF
}

function main {
    clear;
    target=$(realpath "$1");
    base="generated_materials";
    file=${target}/models/${base}.mtl
    max=100;
    echo "$(date +"[%Y-%m-%d %H:%M:%S]") Generating '${file}'..."
    header ${max} >"${file}";
    for i in $(seq 0 ${max}); do
        echo -e -n "$(date +"[%Y-%m-%d %H:%M:%S]") Processing 1/${i}/${max}\r";
        block "mat${i}" >>"${file}";
        texture "${target}/models/textures/" "mat" "${i}";
    done
    texture "${target}/models/textures/" "mat" "0"
    echo "";

#    object=${target}/models/${base}.obj
#    object_header "${base}" >"${object}"
#    cubes=119;
#    for i in $(seq 0 ${cubes}); do
#        echo -e -n "$(date +"[%Y-%m-%d %H:%M:%S]") Processing 2/${i}/${cubes}\r";
#        cube "${i}" 894000 900000 900000 894000 894000 900000 900000 894000 >>"${object}"
#
#        face "${i}" 9 15 7 10 16 7 11 17 7 12 18 7  >>"${object}"
#        face "${i}" 13 19 8 12 18 8 11 20 8 14 21 8 >>"${object}"
#        face "${i}" 14 22 9 11 23 9 10 24 9 15 25 9 >>"${object}"
#        face "${i}" 15 26 10 16 27 10 13 19 10 14 28 10 >>"${object}"
#        face "${i}" 16 27 11 9 15 11 12 18 11 13 19 11 >>"${object}"
#        face "${i}" 15 25 12 10 24 12 9 15 12 16 27 12 >>"${object}"
#    done
#    echo "";
    echo "$(date +"[%Y-%m-%d %H:%M:%S]") Done."
}

main "$@";
