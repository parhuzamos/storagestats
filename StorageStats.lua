NAME = "StorageStats"
VERSION_NUM=5

function SteamDetails()
    local desc = "Place this sign next to any storage to see on a graph how the stored quantity changed in the last 5 minutes. \r\n" ..
					"Connect the sign by placing it next to a storage (1 tile left, top, right, bottom). \r\n" ..
					"After the broken link disappears the storage and the sign are connected, a graph for that storage will be displayed. \r\n" ..
					"You may move the sign elsewhere(!), they are connected forever and title of the sign contains the storage's name. \r\n" ..
					"After connecting, the sign doesn't need to be placed next to the storage anymore. \r\n" ..
					"Any number of signs can be connected to a storage (and placed anywhere). \r\n" ..
					"\r\n" ..
					"* Steam Workshop: https://steamcommunity.com/sharedfiles/filedetails/?id=2747077746 \r\n" ..
					"* Discussion on reddit: https://www.reddit.com/r/Autonauts/comments/snjpz8/storagestats_mod_to_display_a_graph_next_to_your/ \r\n" ..
					"* Chat on Discord: https://discord.com/channels/331886561044201473/647074670822162440/940592994355146832 \r\n" ..
					" \r\n" ..
					"https://gitlab.com/parhuzamos/storagestats"
    ModBase.SetSteamWorkshopDetails(NAME.." v1.2", desc, {"storage", "script", "statistics", "stats"}, "StorageStats.jpg")
end


GRAPH_RESOLUTION = 30
MINUTES=5
SECONDS_PER_SAMPLE = 10
PERCENT_LOW_QUANTITY = 10
PERCENT_HIGH_QUANTITY = 90
BACKPLATE="Backplate"
BASEPLATE="Baseplate"
MATERIAL_GENERATED="generated_materials"
MATERIALS ="materials"
SAVEDATA_LINKS="linkedStorageByStatSignId"
KEY_FIX_ALREADY_ROTATED ="fixAlreadyRotated"
--- At the first release (v1.0) the rotation for the sign was incorrect.
--- Now it's rotated initially at Creation() to fix this problem but only once
--- for players who already saved their game with v1.0. Let's rotate the sign
--- if KEY_FIX_ALREADY_ROTATED does not say otherwise.
fixAlreadyRotated=false

mapWidth = 0
mapHeight = 0

updateFrequencySeconds = 1

rotatingIndex = 1
rotatingCubeNames = { "BaseLinkRight", "BaseLinkBottom", "BaseLinkLeft", "BaseLinkTop"}

statsById = {
	stat = {
		storageId = 0,

	}
}
linkedStorageByStatSignId = {}

function fixForV4()
	fixAlreadyRotated = ModSaveData.LoadValue(KEY_FIX_ALREADY_ROTATED, fixAlreadyRotated)
	if (fixAlreadyRotated ~= "True") then
		for statSignId, _ in pairs(linkedStorageByStatSignId) do
			local rotation = ModBuilding.GetRotation(statSignId)
			if rotation == 0 then
				rotation = 2
			elseif rotation == 1 then
				rotation = 3
			elseif rotation == 2 then
				rotation = 0
			elseif rotation == 3 then
				rotation = 1
			end
			ModBuilding.SetRotation(statSignId, rotation)
		end
	end
	--- Set that the sign were rotated, we don't need to rotate them again.
	--- This will be saved with the savegame and loaded next time with the value
	--- of true, so this will not run again.
	fixAlreadyRotated = true
end

materialNameForBuildingCube = {}

function updateNodeMaterial(buildingId, cubeName, materialName)
	--ModDebug.Log(os.clock(), " updateNodeMaterial ", id, ", ", cubeName, ", ", materialName)

	--- If this cube had not set its material yet, then use default name
	--- from the .obj file: Cube23-Material
	--- It's expected, that the .obj file has material names like this: <cube-name>-Material
	if materialNameForBuildingCube[buildingId] == nil then
		materialNameForBuildingCube[buildingId] = {}
	end
	if materialNameForBuildingCube[buildingId][cubeName] == nil then
		materialNameForBuildingCube[buildingId][cubeName] = cubeName.."-Material"
	end
	--- Get the last set material's name
	local lastMaterialName = materialNameForBuildingCube[buildingId][cubeName]
	--ModObject.SetNodeMaterial(buildingId, cubeName, materialName)

	--- Update the correct material slot
	ModObject.SetNodeMaterial(buildingId, cubeName, materialName, lastMaterialName)
	--- Store the new material's name for the cube in the given object
	materialNameForBuildingCube[buildingId][cubeName] = materialName

	--ModObject.SetNodeMaterial(buildingId, cubeName, materialName, ".*")
end


function ChangeUpdateFrequency(value, name)
	updateFrequencySeconds = value
end

function Expose()
	ModBase.ExposeVariable("Update frequency (seconds)", 1, ChangeUpdateFrequency, 1, 120)
end

function Creation()
	ModBuilding.CreateBuilding(NAME, {"Log"}, {1}, NAME,	{0, 0},	{0, 0}, {}, true)
	ModBuilding.SetBuildingWalkable(NAME, true)
	ModBuilding.UpdateModelScale(NAME, 1.5)
	ModBuilding.UpdateModelRotation(NAME, 0, 0, 0)

	ModObject.AddMaterialsToCache(NAME)
	ModObject.AddMaterialsToCache(MATERIALS)
	ModObject.AddMaterialsToCache(MATERIAL_GENERATED)
end

function AfterLoad()
	mapWidth = ModTiles.GetTilesWide() - 1
	mapHeight = ModTiles.GetTilesHigh() - 1

	local loaded = ModSaveData.LoadValue(SAVEDATA_LINKS, nil)
	if (loaded ~= nil) then
		for statSignIdStr, storageIdStr in string.gmatch(loaded, "(%w+)=(%w+)") do
			connectStatSignToStorage(tonumber(statSignIdStr), tonumber(storageIdStr))
		end
	end

	fixForV4()

	ModTimer.SetCallback(timedUpdate, updateFrequencySeconds*1000, true)
	--ModDebug.Log(os.clock(), " AfterLoad ")
end

function AfterSave()
	--ModDebug.Log(os.clock(), " AfterSave begin")
	local links = ""
	for statSignId, storageId in pairs(linkedStorageByStatSignId) do
		links = links..statSignId.."="..storageId..","
	end
	ModSaveData.SaveValue(SAVEDATA_LINKS, links)
	ModSaveData.SaveValue(KEY_FIX_ALREADY_ROTATED, fixAlreadyRotated)

	--ModDebug.Log(os.clock(), " AfterSave end")
end

function currentMinute()
	return math.floor(os.clock() / SECONDS_PER_SAMPLE)
	--return math.floor(os.clock())
end

function updateCurrentMinute(stats, capacity, quantity)
	local percent = math.floor(quantity / capacity * 100)
	local now = currentMinute()
	local curr = stats[now]
	if (curr == nil) then
		local first = now-GRAPH_RESOLUTION
		if first > 0 then
			stats[first] = nil
		end
		curr = percent
	end
	local v = math.floor((curr + percent) / 2)
	--ModDebug.Log(os.clock(), " percent: ", percent, " capacity: ", capacity, "quantity: ", quantity)
	stats[now] = v
end

--- After pressing '[L] = Properties' these transparent materials are shown as black,
--- this reassigns the transparent material to get rid of the black.
function setTransparentMaterials(statSignId)
	for _, name in pairs(rotatingCubeNames) do
		updateNodeMaterial(statSignId, name, MATERIALS .."/transparent", name.."-Material")
	end
end

function connectStatSignToStorage(statSignId, storageId)
	if (ModObject.IsValidObjectUID(statSignId) and ModObject.IsValidObjectUID(storageId)) then
		--ModDebug.Log(os.clock(), " connectStatSignToStorage ", statSignId, ",", storageId)
		updateNodeMaterial(statSignId, BASEPLATE, MATERIALS .."/connected", BASEPLATE.."-Material")
		linkedStorageByStatSignId[statSignId] = storageId

		for i=1,GRAPH_RESOLUTION do
			updateNodeMaterial(statSignId, "Cube"..i, MATERIAL_GENERATED.."/mat0", "Material")
		end

		setTransparentMaterials(statSignId)
	end
end

function disconnectStatSignFromStorage(statSignId)
	if (linkedStorageByStatSignId[statSignId] ~= nil) then
		updateNodeMaterial(statSignId, BASEPLATE, MATERIALS .."/not_connected", BASEPLATE.."-Material")
		for i=1,GRAPH_RESOLUTION do
			updateNodeMaterial(statSignId, "Cube"..i, MATERIAL_GENERATED.."/mat0", "Material")
		end
		linkedStorageByStatSignId[statSignId] = nil
	end
end

function calcStats(statSignId, storageId)
	local info = ModStorage.GetStorageInfo(storageId)
	if info[3] == nil then
		return
	end
	local stats = statsById[statSignId]
	if (stats == nil) then
		statsById[statSignId] = {}
		stats = statsById[statSignId]
	end
	updateCurrentMinute(stats, info[3], info[2])
	local now = currentMinute()
	local count = 0
	local lastKey = 0
	local lastPercent = 0
	for key, percent in pairs(stats) do
		count = count + 1
		if (key > lastKey) then
			lastKey = key
			lastPercent = percent
		end
	end
	--ModDebug.Log(os.clock(), " calcStats count: ", count)
	for k, percent in pairs(stats) do
		i=GRAPH_RESOLUTION-(now-k)
		if i>0 then
			--ModDebug.Log(os.clock(), " UpdateStorageStats, count: ", count, " capacity:", info[3], " quantity: ", info[2], " key:", k, "=", i, "-> value:", v)
			updateNodeMaterial(statSignId, "Cube"..i, MATERIAL_GENERATED.."/mat"..percent, "Material")
		else
			stats[k] = nil
		end
	end
	if count > 1 then
		if lastPercent <= PERCENT_LOW_QUANTITY then
			updateNodeMaterial(statSignId, BACKPLATE, MATERIALS.."/percent_low", BACKPLATE.."-Material")
		elseif lastPercent >= PERCENT_HIGH_QUANTITY then
			updateNodeMaterial(statSignId, BACKPLATE, MATERIALS.."/percent_high", BACKPLATE.."-Material")
		else
			updateNodeMaterial(statSignId, BACKPLATE, MATERIALS.."/percent_default", BACKPLATE.."-Material")
		end
	end
end

function rotateLinks(statSignId)
	for i, name in pairs(rotatingCubeNames) do
		local materialName = MATERIALS .."/transparent"
		if i == rotatingIndex then
			materialName = MATERIALS.."/LinkTexture"
		end
		updateNodeMaterial(statSignId, name, materialName, name.."-Material")
	end
end

function findStorageNearBy(statSignId)
	local coord = ModObject.GetObjectTileCoord(statSignId)
	for _, xy in pairs({{-1,0}, {1,0}, {0,-1}, {0, 1}, }) do
		local id = ModBuilding.GetBuildingCoveringTile(coord[1]+xy[1], coord[2]+xy[2])
		if (id ~= -1 and ModStorage.IsStorageUIDValid(id)) then
			return id
		end
	end
	return -1
end

function updateSignName(statSignId, storageId)
	local props = ModObject.GetObjectProperties(storageId)
	local storageName = props[5]
	ModBuilding.SetBuildingName(statSignId, NAME..": "..storageName)
end

function timedUpdate()
	--ModDebug.Log(os.clock(), " UpdateStorageStats start")

	if (rotatingIndex < #rotatingCubeNames) then
		rotatingIndex = rotatingIndex + 1
	else
		rotatingIndex = 1
	end

	local statSignIds = {}
	statSignIds = ModBuilding.GetBuildingUIDsOfType(NAME, 0,0, mapWidth, mapHeight)
	for _, statSignId in ipairs(statSignIds) do
		-- is this sign connected already?
		local storageId = linkedStorageByStatSignId[statSignId]
		if (storageId == nil) then
			storageId = findStorageNearBy(statSignId)
			--ModDebug.Log(os.clock(), "  next tile's storageId: ", storageId)
			if (storageId ~= -1) then
				connectStatSignToStorage(statSignId, storageId)
				calcStats(statSignId, storageId)
				updateSignName(statSignId, storageId)
			else
				rotateLinks(statSignId)
				--disconnectStatSignFromStorage(statSignId)
			end
		else
			calcStats(statSignId, storageId)
			updateSignName(statSignId, storageId)
			setTransparentMaterials(statSignId)
		end
	end

	--ModDebug.Log(os.clock(), " UpdateStorageStats end")
end

--function OnUpdate() end

