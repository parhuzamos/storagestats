The `source` directory contains symlink to those files that have to be published.
Switch to `prod` using `$ make prod` to have only the necessary files in the game 
and press "Upload to Steam" inside the game to upload to the Steam Workshop.
